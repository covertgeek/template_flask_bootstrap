Basic Flask App
=========

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores, deleniti, accusamus est provident voluptatum voluptates distinctio odit sit ad dignissimos.

Installation
------------

Install all prerequisites using
> pip install -r requirements.txt

The sqlite database must also be created before the application can run, and the `db_create.py` script takes care of that.

Running
-------

To run the application in the development web server just execute `run.py` with the Python interpreter from the flask virtual environment.

